# 原文链接https://www.codenong.com/cs106009121/

### 一、nlbwmon的安装与简介

使用Openwrt后，我们自然希望它拥有更多的高级功能，这其中就包括对于数据流量的统计。

我们知道Openwrt基于Linux系统，而Linux系统自带了简易的带宽及流量统计功能，但该功能十分“简陋”，保存的数据在系统重启之后便丢失了，并且也没有针对设备、日期、类型的归类统计功能，为此，我们需要使用第三方的工具——nlbwmon。

本文章针对编译时已经安装了nlbwmon的斐讯K3路由器，其它设备可通过opkg自行安装尝试。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200508233345487.png)

如图所示为Luci下的nlbwmon统计展示页面

nlbwmon实质上应当是监控从CPU软转发的NAT数据包，如果传输发生在局域网之中，就无法检测到相关数据了。

因此，无法统计具备硬件NAT模块的路由器上的硬NAT流量。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/2020051109572210.png)

如图所示为nlbwmon的配置界面

默认状态下，数据库目录为/tmp/lib/nlbwmon文件夹，该文件夹在每次系统重启之后均会被自动清空，而有时路由器不可避免的需要进行重启，这会导致流量统计数据丢失，不利于我们掌握流量使用情况，因此，我们将数据库目录更改为具备持久性存储功能的区域并勾选压缩，在图中，将/tmp路径删除，数据库保存在/lib/nlbwmon文件夹。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511100844760.png)

如图所示为保存的数据库文件，以.db.gz格式保存压缩后的数据库

接下来我们可以更改提交到数据库的时间以减少数据丢失的可能性。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511103140640.png)

如图所示可以设置为2h提交一次

由于K3路由器使用的是128MB的SLC闪存，寿命极长，写入寿命可达万次以上，因此可以适当减少数据提交的间隔。

### 二、解决图表加载缓慢的问题

使用nlbwmon的Luci界面时，相信大家可能会发现即使在内网打开，统计界面的图形和数据加载都十分缓慢，有时甚至会加载失败，为了探究原因，我们使用chrome浏览器的开发者工具看看瓶颈究竟在哪里。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511104106606.png)

如上图所示，有一个大文件oui.json的加载引起了我们的注意。

可以看出，该文件请求了GitHub上的项目文件，而国内对于GitHub的访问速度向来存在着较大的问题，因此导致加载缓慢

为此，我们试试能否通过修改nlbwmon的配置文件来重定向该文件。

首先，打开https://raw.githubusercontent.com/jow-/oui-database/master/oui.json或https://lanzous.com/ich705g下载oui.json文件，可以看到，该文件中记录了大量的设备厂商信息，因此体积十分庞大。
![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511104815362.png)

oui.json文件的内容

### 新版文件目录已更改

方法一：

我们可以使用SSH或WinScp登录到路由器找到此目录下的display.js并打开，

目录为/overlay/upper/www/luci-static/resources/view/nlbw/display.js

查找替换'https://raw.git******'里的内容为“https://gitee.com/nothesong/nlbwmon/raw/5c4a2ae358c172d7c2c7a509329516c27277c9b0/oui.json”

或者

“https://gitee.com/nothesong/nlbwmon/releases/download/20240110/oui.json”

后保存，之后清除浏览器缓存后刷新页面即可。

方法二：

使用winscp软件把下载的oui.json文件上传到“/www/luci-static/resources/”目录下然后打开

/overlay/upper/www/luci-static/resources/view/nlbw/display.js

文件替换'https://*******'单引号里面的内容整体替换为以下

http://10.0.0.1/luci-static/resources/oui.json

"10.0.0.1"自己更改为你的路由器后台地址
保存后刷新浏览器缓存

### 以下为旧版文件目录修改教程
下载完成之后，我们可以使用SSH或WinScp登录到路由器，将文件传输至内存中。

为了能使浏览器正确加载，将其放置于/www目录下的luci-static文件夹中，如下图所示。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511105154892.png)

oui.json文件已上传至/www/luci-static目录下

由于在Luci中找不到关于此项的配置，那么接下来我们打开nlbwmon的配置文件看看。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511111104686.png)

配置文件中依旧找不到于此有关的设置项

寻找了启动脚本、软件包配置后依旧找不到相关的设置项，那么，设置项目究竟在哪里呢？

打开网页，我们可以看到下面的地址：

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511114321823.png)

看来，这个页面与“display”有关，

接下来，我们使用WinScp搜索整个文件系统下名为display的文件。

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511111709593.png)

可以看到，这两个文件与网页的路径十分吻合，应当是我们要寻找的文件，事实上，这两个文件指向的都是同一个“display.htm”文件，修改其中任意一个即可。

打开/usr/lib/lua/luci/view/nlbw/display.htm文件并在其中查找字符串“raw”

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511111936495.png)

可以看到，完整的文件路径就保存在该htm文件中

接下来我们对其进行修改，如果要求在公网访问时依然能够正常加载，那么此处的地址需要替换为http(s)://yourdomain/luci-static/oui.json，如果只是在内网访问，直接更改为内网IP地址即可，保存文件后我们清空浏览器缓存再次打开页面

![输入图片说明](https://i2.wp.com/img-blog.csdnimg.cn/20200511112642635.png)

可以看到加载速度有了极大的提升，200ms左右即加载完成

修改到此结束，其它功能等待之后继续更新......